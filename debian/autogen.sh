#!/bin/sh

run()
{
    $@
    if test $? -ne 0; then
	echo "Failed $@"
	exit 1
    fi
}

# for old intltoolize
if [ ! -d config/po ]; then
    mkdir -p config
    ln -s ../po config/po
fi
run mkdir -p m4

run intltoolize --force --copy
run gtkdocize --copy
run libtoolize --copy --force
run aclocal -I m4 $ACLOCAL_OPTIONS
run autoheader
run automake --add-missing --foreign --copy
run autoconf
# run ${AUTORECONF:-autoreconf} --force --install --verbose
